//
//  StringExt.swift
//  Pods
//
//  Created by 石川 諒 on 2017/06/04.
//
//

extension String {
    var rangeOfCharacters: Range<String.Index> {
        // The range of self is always succeeded.
        return self.range(of: self)!
    }
    
    // Convert `Range` to `NSRange`
    func nsRange(from range: Range<String.Index>) -> NSRange {
        return NSRange(range, in: self)
    }
    
    // Returns matched range list by regular expression.
    func matched(by regex: String) -> [Range<String.Index>] {
        let result = try? NSRegularExpression(pattern: regex, options: [])
            .matches(in: self, options: [], range: NSRange(location: 0, length: self.count))
            .compactMap{ $0.range(at: 0).range(for: self) }
        return result ?? []
    }
}
